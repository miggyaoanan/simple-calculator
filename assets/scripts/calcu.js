
// the calculator object holds all the data that we need to construct a valid expression.
const calculator = {
	displayValue: '0',
	firstOperand : null,
	waitingForSecondOperand: false,
	operator: null,

};


//this is to update the display

function updateDisplay() {
	const display =  document.querySelector('.calculator-screen');
	display.value = calculator.displayValue;
	}
	//updateDisplay();

// handling the key presses

const keys = document.querySelector('.calculator-keys');
keys.addEventListener('click', (event) => {

	const { target } =  event;
	if (!target.matches('button')){ return;} // exit the function
	if (target.classList.contains('operator')){
		//console.log('operator', target.value);
		handleOperator(target.value);
		updateDisplay();
		return;
	}

	if (target.classList.contains('decimal')){
		//console.log('decimal', target.value);
		inputDecimal(target.value);
		updateDisplay();
		return;
	}
	if (target.classList.contains('all-clear')){
		//console.log('clear', target.value);
		resetCalculator();
		updateDisplay();
		return;
	}
	//console.log('digit', target.value);
	inputDigit(target.value);
	updateDisplay();
});

// inputting the digits

function inputDigit(digit) {
	const {displayValue, waitingForSecondOperand} = calculator;
	//overwrite to 'displayValue' if the current value is '0' otherwise append to it

	if (waitingForSecondOperand === true) {
		calculator.displayValue = digit;
		calculator.waitingForSecondOperand =false;
	} else {
		calculator.displayValue = displayValue === '0' ? digit : displayValue + digit;
	}

	//console.log(calculator);
}

//inputting a decimal point

function inputDecimal(dot) {
	// if the 'displayValue' does not contain a decimalpoint
	if (calculator.waitingForSecondOperand === true) return;
	if (!calculator.displayValue.includes(dot)){
		//append the decimal point
		calculator.displayValue += dot;
	}
}

// handling Operators

// When the user finishes entering the first operand and hits and operator
/* store the first operand and update the display with the new string of numbers */

function handleOperator(nextOperator) {
	const {firstOperand, displayValue, operator} = calculator
	const inputValue = parseFloat(displayValue);

	if (operator && calculator.waitingForSecondOperand) {
		calculator.operator = nextOperator;
		//console.log(calculator);
		return;
	}

	if (firstOperand === null){
		calculator.firstOperand = inputValue;
	}
	else if (operator){
		const currentValue = firstOperand || 0;
		const result = performCalculation[operator](currentValue, inputValue);

		calculator.displayValue = String(result);
		calculator.firstOperand = result;
	}
	calculator.waitingForSecondOperand = true;
	calculator.operator = nextOperator;
	console.log(calculator);

	return;
}



const performCalculation = {

	'/': (firstOperand, secondOperand) => firstOperand / secondOperand,
	'*': (firstOperand, secondOperand) => firstOperand * secondOperand,
	'+': (firstOperand, secondOperand) => firstOperand + secondOperand,
	'-': (firstOperand, secondOperand) => firstOperand - secondOperand,
	'=': (firstOperand, secondOperand) => secondOperand

	}

function resetCalculator() {
	calculator.displayValue = '0';
	calculator.firstOperand = null;
	calculator.waitingForSecondOperand = false;
	calculator.operator = null;
	//console.log(calculator);

}
